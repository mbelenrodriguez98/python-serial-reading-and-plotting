
import numpy as np


def s16(value):
    return -(value & 0x8000) | (value & 0x7fff)

def s12(value):
    return -(value & 0x800) | (value & 0x7ff)

def fixed_to_float(value):

    mask = []

    mask.append(0x0001)
    mask.append(0x0002)
    mask.append(0x0004)
    mask.append(0x0008)
    mask.append(0x0010)
    mask.append(0x0020)
    mask.append(0x0040)
    mask.append(0x0080)
    mask.append(0x0100)
    mask.append(0x0200)
    mask.append(0x0400)
    mask.append(0x0800)
    mask.append(0x1000)
    mask.append(0x2000)
    mask.append(0x4000)
    mask.append(0x8000)

    n = -15
    suma = 0
    for a in range(15):
        if (value & mask[a]):
            suma += 2**(n)
        n += 1

    if (value & mask[15]):
        suma -= 1.0

    return suma

def fixed_to_float12(value):

    mask = []

    mask.append(0x001)
    mask.append(0x002)
    mask.append(0x004)
    mask.append(0x008)
    mask.append(0x010)
    mask.append(0x020)
    mask.append(0x040)
    mask.append(0x080)
    mask.append(0x100)
    mask.append(0x200)
    mask.append(0x400)
    mask.append(0x800)

    n = -11
    suma = 0
    for a in range(11):
        if (value & mask[a]):
            suma += 2**(n)
        n += 1

    if (value & mask[11]):
        suma -= 1.0

    return suma

def write_coefs():
    coeffs = bytearray()
    
    for a in range (29):
        coeffs.append(0x00)

    #Coef 0: 0xf729
    coeffs.append(0xff)
    coeffs.append(0x7f) #0.111 1111 1111

    '''

    #Coef 0: 0xf729
    coeffs.append(0x29)
    coeffs.append(0xf7)

    #Coef 1: 0xfabf
    coeffs.append(0xbf)
    coeffs.append(0xfa)

    coeffs.append(0xae)
    coeffs.append(0x00)

    coeffs.append(0x37)
    coeffs.append(0x0e)

    coeffs.append(0x7f)
    coeffs.append(0x22)

    coeffs.append(0xbe)
    coeffs.append(0x39)

    coeffs.append(0x46)
    coeffs.append(0x4e)

    coeffs.append(0x50)
    coeffs.append(0x5a)

    coeffs.append(0x50)
    coeffs.append(0x5a)

    coeffs.append(0x46)
    coeffs.append(0x4e)

    coeffs.append(0xbe)
    coeffs.append(0x39)

    coeffs.append(0x7f)
    coeffs.append(0x22)

    coeffs.append(0x37)
    coeffs.append(0x0e)

    coeffs.append(0xae)
    coeffs.append(0x00)

    coeffs.append(0xbf)
    coeffs.append(0xfa)

    coeffs.append(0x29)
    coeffs.append(0xf7)'''

    '''
    valores para debug (letras mayúsculas)
    coeffs.append(0x41)
    time.sleep(0.00001)
    coeffs.append(0x42)
    time.sleep(0.00001)
    coeffs.append(0x43)
    time.sleep(0.00001)
    coeffs.append(0x44)
    time.sleep(0.00001)
    coeffs.append(0x45)
    time.sleep(0.00001)
    coeffs.append(0x46)
    time.sleep(0.00001)
    coeffs.append(0x47)
    time.sleep(0.00001)
    coeffs.append(0x48)
    time.sleep(0.00001)
    coeffs.append(0x49)
    time.sleep(0.00001)
    coeffs.append(0x4A)
    time.sleep(0.00001)
    coeffs.append(0x4B)
    time.sleep(0.00001)
    coeffs.append(0x4C)
    time.sleep(0.00001)
    coeffs.append(0x4D)
    time.sleep(0.00001)
    coeffs.append(0x4E)
    time.sleep(0.00001)
    coeffs.append(0x4F)
    time.sleep(0.00001)
    coeffs.append(0x50)
    time.sleep(0.00001)
    coeffs.append(0x51)
    time.sleep(0.00001)
    coeffs.append(0x50)
    time.sleep(0.00001)
    coeffs.append(0x4F)
    time.sleep(0.00001)
    coeffs.append(0x4E)
    time.sleep(0.00001)
    coeffs.append(0x4D)
    time.sleep(0.00001)
    coeffs.append(0x4C)
    time.sleep(0.00001)
    coeffs.append(0x4B)
    time.sleep(0.00001)
    coeffs.append(0x4A)
    time.sleep(0.00001)
    coeffs.append(0x49)
    time.sleep(0.00001)
    coeffs.append(0x48)
    time.sleep(0.00001)
    coeffs.append(0x47)
    time.sleep(0.00001)
    coeffs.append(0x46)
    time.sleep(0.00001)
    coeffs.append(0x45)
    time.sleep(0.00001)
    coeffs.append(0x44)
    time.sleep(0.00001)
    coeffs.append(0x43)
    time.sleep(0.00001)
    coeffs.append(0x42)
    time.sleep(0.00001)
    '''
    return(coeffs)
