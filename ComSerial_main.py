
#%%

# Al ejecutar el código, se va a poner a escuchar por el puerto serial.
# Revisar puerto y paridad antes de ejecutar
# Toma los datos recibidos y los plotea en formato decimal

import codecs
import serial
import matplotlib.pyplot as plt
import numpy as np
import fft_plotter
import utils
from numpy import pi
from scipy.fftpack import fft, fftfreq
        

# Configuración de puerto serial
serialPort = serial.Serial(port = "COM7", baudrate=115200,
                           bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE, parity=serial.PARITY_ODD)

results1 = []
results2 = []

coeffs = bytearray()
coeffs = utils.write_coefs()

serialPort.write(coeffs)

while(1):

    # Espero a que haya datos en el puerto serial
    if(serialPort.in_waiting > 0):

        # Leer datos seriales hasta completar 8200 datos
        serialString = serialPort.read_until(expected=' ',size=4096)
        print(len(serialString))
        binaryString = codecs.encode(serialString, "hex")
        #print(binaryString)
        print(len(binaryString))

        break

a=0
while (a < (len(binaryString)-2)):
    try:
        
        qword1=utils.s16(int(binaryString[a:a+4],16))
        qword2=utils.fixed_to_float(int(binaryString[a:a+4],16))

        print(binaryString[a:a+4],qword1,qword2)
        results1.append(qword1)
        results2.append(qword2)
    except(ValueError):
        print('error')
        a=a+4
        continue
    a=a+4

plt.plot(results2)
plt.ylabel('Resultados - Float')
plt.show()

plt.plot(results1)
plt.ylabel('Resultados - Int')
plt.show()

fft_plotter.plot_fft(results2)
# %%
