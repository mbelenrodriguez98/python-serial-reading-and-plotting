# Python serial reading and plotting

Recomendado correr con Visual Studio Code. Esta es la explicación para Win pero es transladable a Linux.
Luego de instalar Python3, lo primero que habría que hacer es crear un entorno virtual.
Para ello busco el directorio deseado desde el terminal (puede ser el del mismo Visual) y:

```
python3 -m venv **nombredemientorno**
**nombredemientorno**\scripts\activate.bat
```
Una vez creado el entorno, desde el directorio donde está el repositorio:
> pip install -r requirements.txt

Una vez hecho esto ya está listo para ejecutarse el código. 
En caso de querer probarlo sin otros dispositivos, se pueden hacer pruebas con el Virtual Serial Port Driver
https://www.virtual-serial-port.org/vspd-post-download.html

