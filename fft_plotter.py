# Transformadas Rapida de Fourier
# entrada funcion par

import numpy as np
import scipy.fftpack as fourier
import matplotlib.pyplot as plt

# Definir la funcion de Entrada par
def plot_fft(xanalog):

    n=200
    dt=1/100000

    # FFT: Transformada Rapida de Fourier
    # Analiza la parte t>=0 de xnalog muestras[n:2*n]
    xf=fourier.fft(xanalog[n:2*n])
    xf=fourier.fftshift(xf)
    # Rango de frecuencia para eje
    frq=fourier.fftfreq(n, dt)
    frq=fourier.fftshift(frq)

    # x[w] real
    xfreal=(1/n)*np.real(xf)
    # x[w] imaginario
    xfimag=(1/n)*np.imag(xf)
    # x[w] magnitud
    xfabs=(1/n)*np.abs(xf)
    # x[w] angulo
    xfangle=(1/n)*np.unwrap(np.angle(xf))

    #SALIDA
    plt.figure(1)       # define la grafica
    plt.suptitle('Transformada Rápida Fourier FFT')

    ventana=0.4 # ventana de frecuencia a observar alrededor f=0
    ra=int(len(frq)*(0.5-ventana))
    rb=int(len(frq)*(0.5+ventana))
    plt.subplot(221)    # grafica de 2x2, subgrafica 1
    plt.ylabel('x[f] real')
    plt.xlabel(' frecuencia (Hz)')
    plt.plot(frq[ra:rb],xfreal[ra:rb])
    plt.margins(0,0.05)
    plt.grid()

    plt.subplot(222)    # grafica de 2x2, subgrafica 2
    plt.ylabel('x[f] imag')
    plt.xlabel(' frecuencia (Hz)')
    plt.plot(frq[ra:rb],xfimag[ra:rb])
    plt.margins(0,0.05)
    plt.grid()

    plt.subplot(223)    # grafica de 2x2, subgrafica 3
    plt.ylabel('x[f] magnitud')
    plt.xlabel(' frecuencia (Hz)')
    plt.plot(frq[ra:rb],xfabs[ra:rb])
    plt.margins(0,0.05)
    plt.grid()

    plt.subplot(224)    # grafica de 2x2, subgrafica 4
    plt.ylabel('x[f] fase')
    plt.xlabel(' frecuencia (Hz)')
    plt.plot(frq[ra:rb],xfangle[ra:rb])
    plt.margins(0,0.05)
    plt.grid()

    plt.show()